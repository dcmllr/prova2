package utfpr.ct.dainf.pratica;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author 1937529
 */
public class Ponto {
    private double x, y, z;

    public Ponto() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    
    
    public Ponto(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public double getX() {
        return x;
    }
    
    public void setX(double x) {
        this.x = x;
    }        
    
    public double getY() {
        return y;
    }        
   
    public void setY(double y) {
        this.y = y;
        
    }
    
    public double getZ(){
        return z;
    }
    
    public void setZ(double z){
        this.z = z;
    }
    /**
     *
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    
    @Override
    public String toString(){
       String a = String.format("%s(%fl, %fl, %fl)", getNome(), getX(), getY(), getZ());
       return a;
    }
    
    /**
     *
     * @param obj
     * @param pt
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        boolean equal = false;
        if (obj != null && obj instanceof Ponto) {
            Ponto pt = (Ponto) obj;
            equal = x == pt.x && y == pt.y && z == pt.z;
        }
        return equal;
    }  
    
    
    public double dist(Ponto pt){
        double distancia = sqrt(pow((this.getX()-pt.getX()),2) + pow((this.getY()-pt.getY()),2) + pow((this.getZ()-pt.getZ()),2));
        return distancia;
    }
            
            
    public String getNome() {
        return getClass().getSimpleName();
    }
    
}
