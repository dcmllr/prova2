import utfpr.ct.dainf.pratica.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 1937529 
 */
public class Pratica {

        public static void main(String[] args) {
        PontoXZ a = new PontoXZ(-3, 2);
        PontoXY b = new PontoXY(0, 2);
        
        double c = a.dist(b);
        
        System.out.println("Distancia = " + c);
    }
    
}
